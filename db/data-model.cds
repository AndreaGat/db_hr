namespace my.hr;
using { User, managed } from '@sap/cds/common';

entity FERIE_DIP {
  key ID_RICHIESTA		: Integer;
  UTENTE				: Association[1] to ANAGRAFICA_DIP {ID};
  DATA_INIZIO			: Date;
  DATA_FINE				: Date;
  GG_FERIE				: Decimal;
  DATA_RICHIESTA		: Date;
  STATUS				: String(1);
  ID_RESPONSABILE		: String(12);
  DATA_MODIFICA_STATUS	: Date;
  NOTE_DIPENDENTE		: String;
  NOTE_RESPONSABILE		: String;
}

entity GIORNI_NOLAV {
  key ID		: Integer;
  DATA_NOLAV	: Date;
  TIPO			: String(1);
  SEDE			: String;
}

entity ANAGRAFICA_DIP {
  key ID		 : String(12);
  ID_UTENTE		 : Association [0..*] to FERIE_DIP on ID_UTENTE.UTENTE = $self;
  COGNOME		 : String;
  NOME			 : String;
  FERIE_INIZIALI : Decimal;
  SEDE			 : String;
  RUOLO 		 : String(1);
  ID_RESPONSABILE: String(12);
}