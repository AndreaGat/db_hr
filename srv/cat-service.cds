using { my.hr, sap.common } from '../db/data-model';

service CatalogService @(requires: ['ShowService','system-user'] ) {
  entity FERIE_DIP as projection on hr.FERIE_DIP;
  entity GIORNI_NOLAV as projection on hr.GIORNI_NOLAV;
  entity ANAGRAFICA_DIP as projection on hr.ANAGRAFICA_DIP;
}